package org.example;

import org.example.model.Part;

public interface PartCommand {
    Part execute(Part part);
}
