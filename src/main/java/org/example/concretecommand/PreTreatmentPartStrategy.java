package org.example.concretecommand;

import org.example.PartCommand;
import org.example.model.Part;
import org.example.receiver.PreTreatmentPartReceiver;

public class PreTreatmentPartStrategy implements PartCommand {

    private PreTreatmentPartReceiver preTreatmentPartReceiver;

    public PreTreatmentPartStrategy(PreTreatmentPartReceiver preTreatmentPartReceiver) {
        this.preTreatmentPartReceiver = preTreatmentPartReceiver;
    }

    @Override
    public Part execute(Part part) {
        return preTreatmentPartReceiver.action(part);
    }
}
