package org.example.concretecommand;

import org.example.PartCommand;
import org.example.model.Part;
import org.example.receiver.PlassticPartReceiver;

public class PlassticPartStrategy implements PartCommand {

    private PlassticPartReceiver plassticPartReceiver;

    public PlassticPartStrategy(PlassticPartReceiver plassticPartReceiver) {
        this.plassticPartReceiver = plassticPartReceiver;
    }

    @Override
    public Part execute(Part part) {
        return plassticPartReceiver.action(part);
    }
}
