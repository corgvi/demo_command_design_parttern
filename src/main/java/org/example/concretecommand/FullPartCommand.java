package org.example.concretecommand;

import org.example.PartCommand;
import org.example.model.Part;
import org.example.receiver.FullPartReceiver;

public class FullPartCommand implements PartCommand {
    private FullPartReceiver fullPartReceiver;

    public FullPartCommand(FullPartReceiver fullPartReceiver) {
        this.fullPartReceiver = fullPartReceiver;
    }

    @Override
    public Part execute(Part part) {
        return fullPartReceiver.action(part);
    }
}
