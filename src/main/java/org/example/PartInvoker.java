package org.example;

import org.example.model.Part;

public class PartInvoker {
    private PartCommand partCommand;

    public void setPartCommand(PartCommand partCommand) {
        this.partCommand = partCommand;
    }

    public Part executeCommand(Part part) {
        return partCommand.execute(part);
    }
}
