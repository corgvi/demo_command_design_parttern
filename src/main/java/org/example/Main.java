package org.example;

import org.example.concretecommand.FullPartCommand;
import org.example.concretecommand.PlassticPartStrategy;
import org.example.concretecommand.PreTreatmentPartStrategy;
import org.example.model.Material;
import org.example.model.Part;
import org.example.receiver.FullPartReceiver;
import org.example.receiver.PlassticPartReceiver;
import org.example.receiver.PreTreatmentPartReceiver;

public class Main {
    public static void main(String[] args) {
        Part part1 = new Part(1, true, "caution 1", "comment 1", "icon path 1", new Material(1, "Copper"));
        Part part2 = new Part(2, false, "caution 2", "comment 2", "icon path 2", new Material(2, "Steel"));
        Part part3 = new Part(3, true, "caution 3", "comment 3", "icon path 3", new Material(3, "Plastic"));

        PreTreatmentPartReceiver preTreatmentPartReceiver = new PreTreatmentPartReceiver();
        PartCommand partCommand = new PreTreatmentPartStrategy(preTreatmentPartReceiver);
        PartInvoker partInvoker = new PartInvoker();
        partInvoker.setPartCommand(partCommand);
        partInvoker.executeCommand(part1);
        System.out.println(partInvoker.executeCommand(part1));

        FullPartReceiver fullPartReceiver = new FullPartReceiver();
        partCommand = new FullPartCommand(fullPartReceiver);
        partInvoker = new PartInvoker();
        partInvoker.setPartCommand(partCommand);
        System.out.println(partInvoker.executeCommand(part2));

        PlassticPartReceiver plassticPartReceiver = new PlassticPartReceiver();
        partCommand = new PlassticPartStrategy(plassticPartReceiver);
        partInvoker = new PartInvoker();
        partInvoker.setPartCommand(partCommand);
        System.out.println(partInvoker.executeCommand(part3));
    }
}